# escape=`

# This assumes that the host has at Qt installed in C:\Qt. Run with
# --volume c:\qt:c:\qt:ro

# Modified from https://code.qt.io/cgit/qbs/qbs.git/plain/docker/windowsservercore/Dockerfile
FROM mcr.microsoft.com/windows/servercore:ltsc2019
# FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-windowsservercore-ltsc2019
LABEL Description="Windows Server Core development environment for Wireshark"

USER ContainerAdministrator

# Disable crash dialog for release-mode runtimes
RUN reg add "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting" /v Disabled /t REG_DWORD /d 1 /f `
    && reg add "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting" /v DontShowUI /t REG_DWORD /d 1 /f

# Install Chocolatey.
RUN powershell -NoProfile -ExecutionPolicy Bypass -Command `
    $Env:chocolateyVersion = '0.11.3' ; `
    $Env:chocolateyUseWindowsCompression = 'false' ; `
    "[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# VS component list:
# https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools

# We have to install Visual C++ 2019 by hand in a container.
# https://stackoverflow.com/a/62953087/82195
RUN powershell -NoProfile -ExecutionPolicy Bypass -Command `
    Invoke-WebRequest "https://aka.ms/vs/16/release/vs_community.exe" `
    -OutFile "%TEMP%\vs2019_community.exe" -UseBasicParsing `
    `
    # --add Microsoft.VisualStudio.Component.VC.CMake.Project `
    # --add Microsoft.VisualStudio.Component.Windows10SDK.19041 `
    && "%TEMP%\vs2019_community.exe" --quiet --wait --norestart --noUpdateInstaller `
    --add Microsoft.VisualStudio.Workload.NativeDesktop `
    --add Microsoft.VisualStudio.Workload.VCTools `
    --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 `
    --add Microsoft.VisualStudio.Component.VC.Redist.MSM

# Install Visual C++ 2022 by hand.
# https://docs.microsoft.com/en-us/visualstudio/install/build-tools-container?view=vs-2022
RUN powershell -NoProfile -ExecutionPolicy Bypass -Command `
    Invoke-WebRequest "https://aka.ms/vs/17/release/vs_community.exe" `
    -OutFile "%TEMP%\vs2022_community.exe" -UseBasicParsing `
    `
    # --add Microsoft.VisualStudio.Component.VC.CMake.Project `
    && ("%TEMP%\vs2022_community.exe" --quiet --wait --norestart --noUpdateInstaller `
    --add Microsoft.VisualStudio.Workload.NativeDesktop.Core `
    --add Microsoft.VisualStudio.Workload.VCTools `
    --add Microsoft.VisualStudio.Component.Windows11SDK.22000 `
    --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 `
    --add Microsoft.VisualStudio.Component.VC.Redist.MSM `
    || IF "%ERRORLEVEL%"=="3010" EXIT 0)

# Install various packages.
# XXX AsciidoctrJ requires a jre.
#    choco install -y corretto11jdk asciidoctorj xsltproc docbook-bundle && `
RUN `
    choco install -y cmake && `
    choco install -y python3 && `
    choco install -y strawberryperl && `
    choco install -y git && `
    choco install -y winflexbison3 && `
    choco install -y 7zip && `
    setx /M PATH "%PATH%;C:\Program Files\CMake\bin" && `
    setx /M PATH "%PATH%;C:\Strawberry\c\bin;C:\Strawberry\perl\site\bin;C:\Strawberry\perl\bin" && `
    setx /M PATH "%PATH%;C:\Program Files\Git\cmd"

# clcache for speeding up MSVC builds
# ENV CLCACHE_DIR="C:/.ccache"

# Expose the Qt directory.
VOLUME C:\Qt
