# Wireshark-Related Docker Images

This repository contains various Docker images related to [Wireshark](https://www.wireshark.org).

See the READMEs in each [subdirectory of this repository](https://gitlab.com/wireshark/wireshark-containers/-/tree/master) for more information.

The images under [dev](https://gitlab.com/wireshark/wireshark-containers/-/tree/master/dev) are used for [CI in the main repository](https://gitlab.com/wireshark/wireshark/-/blob/master/.gitlab-ci.yml).

# Registry

Images are available at https://gitlab.com/wireshark/wireshark-containers/container_registry.
You can access the latest version of each image using the “latest” tag or past versions using a quarter tag such as “2021-Q4”.

# Contributing

Please submit patches to
https://gitlab.com/wireshark/wireshark-containers.

Patch submission documentation can be found at
https://www.wireshark.org/docs/wsdg_html_chunked/ChSrcContribute.html
and
https://gitlab.com/wireshark/wireshark/-/wikis/Development/SubmittingPatches.